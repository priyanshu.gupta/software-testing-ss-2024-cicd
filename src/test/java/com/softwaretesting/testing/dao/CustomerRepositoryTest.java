package com.softwaretesting.testing.dao;

import com.softwaretesting.testing.model.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class CustomerRepositoryTest {
    @Autowired
    CustomerRepository customerRepository;

    private Customer customer;

    @BeforeEach
    void setup(){
        customer = new Customer(Long.parseLong("1000"),"test_1","Test Customer 1","+4915112345678");
    }

    @Test
        // Test to verify if a new customer can be saved and returned by the repository
    void save_WhenNewCustomerSaved_CustomerIsPersistedAndReturned(){
        // Act
        Customer savedCustomer = customerRepository.save(customer);

        // Assert
        assertNotNull(savedCustomer.getId());
        assertEquals(customer.getUserName(), savedCustomer.getUserName());
    }

    @Test
        // Test to verify if a list of all customers in the database can be retrieved
    void findAll_ReturnsListOfAllCustomers(){
        // Arrange
        customerRepository.save(customer);
        Customer c2 = new Customer(Long.parseLong("1001"),"test_2","Test Customer 2","+4917223456789");
        Customer c3 = new Customer(Long.parseLong("1002"),"test_3","Test Customer 3","+4912312345678");
        customerRepository.save(c2);
        customerRepository.save(c3);

        // Act
        List<Customer> customers = (List<Customer>) customerRepository.findAll();

        // Assert
        assertNotNull(customers);
        assertEquals(3, customers.size());
    }

    @Test
    // Test to verify if a customer can be found by id when the customer exists in the database
    void testRetrieveCustomerById(){
        Customer  savedCustomer = customerRepository.save(customer);
        Long id = savedCustomer.getId();
        Optional<Customer> customerPresent = customerRepository.findById(id);
        assertTrue(customerPresent.isPresent());
        // assertNotNull(customerPresent.get());
        assertEquals(savedCustomer.getId() , customerPresent.get().getId());
    }

    @Test
        // Test to verify if a customer can be found by username when the customer exists in the database
    void findByUserName_WhenExists_ReturnsCustomer(){
        // Arrange
        Customer  savedCustomer = customerRepository.save(customer);
        String savedUserName = savedCustomer.getUserName();

        // Act
        Optional<Customer> found = customerRepository.findByUserName(savedUserName);

        // Assert
        assertTrue(found.isPresent());
        assertNotNull(found.get());
        assertEquals(savedCustomer.getUserName() , found.get().getUserName());
    }

    @Test
        // Test to verify if an empty optional is returned when trying to retrieve a customer by a non-existent username
    void findByUserName_WhenNotExists_ReturnsEmpty() {
        // Act
        Optional<Customer> found = customerRepository.findByUserName("nonexistent");

        // Assert
        assertTrue(found.isEmpty());
    }

    @Test
        // Test to verify if a customer can be found by phone number when the customer exists in the database
    void selectCustomerByPhoneNumber_WhenExists_ReturnsCustomer(){
        // Arrange
        Customer savedCustomer = customerRepository.save(customer);
        String savedPhoneNumber = savedCustomer.getPhoneNumber();

        // Act
        Optional<Customer> found = customerRepository.selectCustomerByPhoneNumber(savedPhoneNumber);

        // Assert
        assertTrue(found.isPresent());
        assertNotNull(found.get());
        assertEquals(savedCustomer.getPhoneNumber() , found.get().getPhoneNumber());
    }

    @Test
        // Test to verify if an empty optional is returned when trying to retrieve a customer by a non-existent phone number
    void selectCustomerByPhoneNumber_WhenNotExists_ReturnsEmpty() {
        // Act
        Optional<Customer> found = customerRepository.selectCustomerByPhoneNumber("0000000000");

        // Assert
        assertTrue(found.isEmpty());
    }

    @Test
    void updateCustomer_WhenCustomerExists_CustomerIsUpdated() {
        // Arrange
        Customer savedCustomer = customerRepository.save(customer);
        savedCustomer.setName("Updated Name");

        // Act
        Customer updatedCustomer = customerRepository.save(savedCustomer);

        // Assert
        assertEquals(savedCustomer.getId(), updatedCustomer.getId());
        assertEquals("Updated Name", updatedCustomer.getName());
    }

    @Test
    void deleteCustomer_WhenCustomerExists_CustomerIsDeleted() {
        // Arrange
        Customer savedCustomer = customerRepository.save(customer);

        // Act
        customerRepository.delete(savedCustomer);

        // Assert
        Optional<Customer> deletedCustomer = customerRepository.findById(savedCustomer.getId());
        assertTrue(deletedCustomer.isEmpty());
    }
}
