package com.softwaretesting.testing.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class CustomerTest {

    @Test
    void testCustomerConstructor() {
        Customer customer = new Customer(1L, "user1", "Name1", "1234567890");
        assertNotNull(customer);
        assertEquals(Long.valueOf(1), customer.getId());
        assertEquals("user1", customer.getUserName());
        assertEquals("Name1", customer.getName());
        assertEquals("1234567890", customer.getPhoneNumber());
    }

    @Test
    void testIdSetterAndGetter() {
        Customer customer = new Customer();
        customer.setId(1L);
        assertEquals(Long.valueOf(1), customer.getId());
    }

    @Test
    void testUserNameSetterAndGetter() {
        Customer customer = new Customer();
        customer.setUserName("user1");
        assertEquals("user1", customer.getUserName());
    }

    @Test
    void testNameSetterAndGetter() {
        Customer customer = new Customer();
        customer.setName("Name1");
        assertEquals("Name1", customer.getName());
    }

    @Test
    void testPhoneNumberSetterAndGetter() {
        Customer customer = new Customer();
        customer.setPhoneNumber("1234567890");
        assertEquals("1234567890", customer.getPhoneNumber());
    }

    @Test
    void testEquals() {
        Customer customer1 = new Customer(1L, "user1", "Name1", "1234567890");
        Customer customer2 = new Customer(1L, "user1", "Name1", "1234567890");
        Customer customer3 = new Customer(2L, "user2", "Name2", "0987654321");

        // Reflexive
        assertEquals(customer1, customer1, "Customer should be equal to itself");

        // Symmetric
        assertEquals(customer1, customer2, "Customers with same properties should be equal");
        assertEquals(customer2, customer1, "Equality should be symmetric");

        // Transitive
        Customer customer4 = new Customer(1L, "user1", "Name1", "1234567890");
        assertEquals(customer1, customer2, "Equality should be transitive");
        assertEquals(customer2, customer4, "Equality should be transitive");
        assertEquals(customer1, customer4, "Equality should be transitive");

        // Null check
        assertNotEquals(customer1, null, "Customer should not be equal to null");

        // Different properties
        assertNotEquals(customer1, customer3, "Customers with different properties should not be equal");
    }

    @Test
    void testHashCode() {
        Customer customer1 = new Customer(1L, "user1", "Name1", "1234567890");
        Customer customer2 = new Customer(1L, "user1", "Name1", "1234567890");

        // Consistent
        assertEquals(customer1.hashCode(), customer1.hashCode(), "HashCode should be consistent");

        // Equal objects have same hashCode
        assertEquals(customer1.hashCode(), customer2.hashCode(), "Equal objects should have same hashCode");

        // Different objects can have different hashCodes (Not strictly necessary, but generally expected)
        Customer customer3 = new Customer(2L, "user2", "Name2", "0987654321");
        assertNotEquals(customer1.hashCode(), customer3.hashCode(), "Different objects can have different hashCodes");
    }

    @Test
    void testToString() {
        Customer customer = new Customer(1L, "user1", "Name1", "1234567890");
        String expected = "Customer{id=1, userName='user1', name='Name1', phoneNumber='1234567890'}";
        assertEquals(expected, customer.toString(), "toString should return the expected string representation");
    }
}
